import 'package:flutter/material.dart';
import 'package:weekly_timetable/weekly_timetable.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Table Schedule',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage(title: 'Flutter TimeTable Plugin Example'),
        debugShowCheckedModeBanner: false);
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Map<int, List<int>> initialSchedule = {
    0: [],
    1: [],
    2: [],
    3: [],
    4: [],
    5: [],
    6: [],
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: WeeklyTimeTable(),
    );
  }
}

class WeeklyTimeTable extends StatefulWidget {
  final ValueChanged<Map<int, List<int>>> onValueChanged;
  final Color cellColor;
  final Color cellSelectedColor;
  final Color boarderColor;
  final Map<int, List<int>> initialSchedule;
  final bool draggable;
  final String locale;

  WeeklyTimeTable({
    this.cellColor = Colors.white,
    this.cellSelectedColor = Colors.black,
    this.boarderColor = Colors.grey,
    this.initialSchedule = const {
      0: [],
      1: [],
      2: [],
      3: [],
      4: [],
      5: [],
      6: [],
    },
    this.draggable = false,
    this.locale = "en",
    this.onValueChanged,
  });

  @override
  _WeeklyTimeTableState createState() =>
      _WeeklyTimeTableState(this.initialSchedule);
}

class _WeeklyTimeTableState extends State<WeeklyTimeTable> {
  String locale = 'en';
  Map<int, List<int>> selected;

  _WeeklyTimeTableState(this.selected);

  @override
  void initState() {
    if (WeeklyTimes.localContains(widget.locale)) {
      setState(() {
        locale = widget.locale;
      });
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Header(WeeklyTimes.dates[this.locale]),
        Expanded(
          child: ListView.builder(
            itemCount: WeeklyTimes.times[this.locale].length,
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemBuilder: (BuildContext context, int index) {
              List<Widget> children = [];
              children.add(Indicator(WeeklyTimes.times[this.locale][index]));
              children.addAll(
                List.generate(
                  WeeklyTimes.dates[this.locale].length - 1,
                  (i) => Cell(
                    day: i,
                    timeRange: index,
                    isSelected: selected[i].contains(index),
                    onCellTapped: onCellTapped,
                    cellColor: widget.cellColor,
                    cellSelectedColor: widget.cellSelectedColor,
                    boarderColor: widget.boarderColor,
                  ),
                ),
              );
              return Row(children: children);
            },
          ),
        ),
      ],
    );
  }

  onCellTapped(int day, int timeRange, bool nextSelectedState) {
    setState(() {
      if (!nextSelectedState) {
        selected[day].add(timeRange);
      } else {
        selected[day].remove(timeRange);
      }
    });
    widget.onValueChanged(selected);
  }
}

class Cell extends StatefulWidget {
  final int day;
  final int timeRange;
  final bool isSelected;
  final Function(int, int, bool) onCellTapped;
  final Color cellColor;
  final Color cellSelectedColor;
  final Color boarderColor;

  Cell({
    @required this.day,
    @required this.timeRange,
    @required this.isSelected,
    @required this.onCellTapped,
    this.cellColor = Colors.white,
    this.cellSelectedColor = Colors.black,
    this.boarderColor = Colors.grey,
  });

  @override
  _CellState createState() => _CellState();
}

class _CellState extends State<Cell> {
  Key key;

  @override
  void initState() {
    key = Key("timecell-${widget.day}-${widget.timeRange}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      key: key,
      child: Center(
        child: GestureDetector(
          onTap: () {
            widget.onCellTapped(
                widget.day, widget.timeRange, widget.isSelected);
          },
          child: AnimatedContainer(
            decoration: BoxDecoration(
              color: currentColor,
              border: Border(
                top: BorderSide(width: 1.0, color: widget.boarderColor),
                left: BorderSide(width: 0.0, color: widget.boarderColor),
                right: BorderSide(width: 0.0, color: widget.boarderColor),
              ),
            ),
            height: 58.0,
            duration: Duration(milliseconds: 500),
            curve: Curves.fastOutSlowIn,
          ),
        ),
      ),
    );
  }

  get currentColor {
    return widget.isSelected ? widget.cellSelectedColor : widget.cellColor;
  }
}

class Header extends StatelessWidget {
  Header(this.dates);

  final List<String> dates;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      child: Card(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 2.0),
        child: Row(
            children: dates
                .map((day) => Expanded(child: Center(child: Text(day))))
                .toList()),
        elevation: 8.0,
      ),
    );
  }
}

class Indicator extends StatelessWidget {
  final String time;

  Indicator(this.time);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Align(
        alignment: Alignment.bottomCenter,
        heightFactor: 0.8,
        child: Container(
          height: 58,
          child: Text(
            '$time',
            style: TextStyle(fontSize: 10.0),
          ),
        ),
      ),
    );
  }
}
